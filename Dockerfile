FROM alpine:3.10

RUN apk update
RUN apk add python3 docker

RUN pip3 install --upgrade pip
RUN pip3 install awscli ecs-deploy

CMD ["/bin/sh"]
